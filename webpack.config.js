const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const PATHS = {
  src: path.join(__dirname, './src'),
  dist: path.join(__dirname, './dist'),
  assets: 'assets/',
};

module.exports = (env = {}) => {
  const { mode = 'development' } = env;
  const isProd = mode === 'production';
  const isDev = mode === 'development';
  const getStyleLoaders = () => [
    isProd ? MiniCssExtractPlugin.loader : 'style-loader',
    {
      loader: 'css-loader',
      options: { sourceMap: true },
    },
  ];
  const getPlugins = () => {
    const plugins = [
      new HtmlWebpackPlugin({
        title: 'Hello World',
        buildTime: new Date().toString(),
        template: 'public/index.html',
      }),
      new webpack.SourceMapDevToolPlugin({
        filename: '[file].map',
      }),
      new webpack.HotModuleReplacementPlugin(),
    ];
    if (isProd) {
      plugins.push(new MiniCssExtractPlugin(
        { filename: 'main-[hash:8].css' },
      ));
    }
    return plugins;
  };


  return {
    mode: isProd ? 'production' : isDev && 'development',
    entry: `${PATHS.src}/index.js`,
    output: {
      filename: isProd ? 'main-[hash:8].js' : undefined,
    },
    optimization: {
      splitChunks: {
        cacheGroups: {
          vendor: {
            name: 'vendors',
            test: /node_modules/,
            chunks: 'all',
            enforce: true,
          },
        },
      },
    },
    resolve: {
      alias: {
        components: path.resolve(__dirname, `${PATHS.src}/components`),
        store: path.resolve(__dirname, `${PATHS.src}/store`),
        assets: path.resolve(__dirname, `${PATHS.src}/assets`),
      },
      extensions: ['*', '.js', '.jsx', '.css', '.scss'],
    },
    module: {
      rules: [
        // js loading
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          loader: ['babel-loader', 'eslint-loader'],
        },
        // css loading
        {
          test: /\.css$/,
          exclude: /node_modules/,
          use: [...getStyleLoaders(),
            {
              loader: 'postcss-loader',
              options: { sourceMap: true },
            },
          ],
        },
        // scss/sass loading
        {
          test: /\.s[ca]ss$/,
          exclude: /node_modules/,
          use: [...getStyleLoaders(),
            {
              loader: 'postcss-loader',
              options: { sourceMap: true },
            },
            {
              loader: 'sass-loader',
              options: { sourceMap: true },
            }],

        },
        // loading images
        {
          test: /\.(png|jpg|jpeg|svg|gif|ico)$/,
          use: [{
            loader: 'file-loader',
            options: {
              outputPath: 'images',
              name: '[name]-[sha1:hash:7].[ext]',
            },
          }],
        },
        // loading fonts
        {
          test: /\.(ttf|otf|eot|woff|woff2)$/,
          use: [{
            loader: 'file-loader',
            options: {
              outputPath: 'fonts',
              name: '[name].[ext]',
            },
          }],
        },
      ],
    },
    devtool: 'cheap-module-eval-source-map',
    plugins: getPlugins(),
    devServer: {
      open: true,
      hot: true,
      overlay: {
        warnings: false,
        errors: true,
      },
    },
  };
};
