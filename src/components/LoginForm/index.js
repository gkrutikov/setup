import React from 'react';
import { Form, Icon, Input, Button} from 'antd';

export class NormalLoginForm extends React.Component {

  render() {
    const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 6 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 16 },
        },
      };
      const tailFormItemLayout = {
        wrapperCol: {
          xs: {
            span: 24,
            offset: 0,
          },
          sm: {
            span: 16,
            offset: 13,
          },
        },
      };
      const buttonFormItemLayout = {
        wrapperCol: {
            sm: {
              span: 16,
              offset: 10,
            },
          },
      };
    return (
      <Form {...formItemLayout} className="login-form">
        <Form.Item label="Логин">
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
            />
        </Form.Item>
        <Form.Item label="Пароль" className="login-form-password-item">
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
            />
        </Form.Item>
        <Form.Item {...tailFormItemLayout} className="login-form-forgot-item">
          <a className="login-form-forgot" href="/">
            Забыли пароль?
          </a>
        </Form.Item>
        <Form.Item {...buttonFormItemLayout} className="login-form-button-item">
          <Button type="primary" htmlType="submit" className="login-form-button">
            Войти
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
