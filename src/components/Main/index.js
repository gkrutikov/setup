import React from 'react'
import { MainBody } from './styles'
import { Card, Layout } from 'antd'
import { NormalLoginForm } from '../LoginForm'
const { Header} = Layout



export const Main = () => {
    return (
        <MainBody>
            <Layout>
                <Header><div class="slogan">Система заказа обедов. Панель администратора.</div></Header>
                <Card className="login-card">
                    <NormalLoginForm />
                </Card>
            </Layout>
        </MainBody>
    )
}
