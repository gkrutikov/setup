import styled from 'styled-components'

export const MainBody = styled.div`
width: 100%;
.slogan {
    color: #fafafa;
    font-size:20px;
}
.ant-card-body
{
    padding-bottom: 0;
}
.login-card {
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
}
.login-form-forgot-item {
    margin-bottom: 0;
}
.login-form-button-item {
    margin-bottom: 0;
}
.login-form-password-item {
    margin-bottom: 0;
}
`