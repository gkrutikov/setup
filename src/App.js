/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { connect } from 'react-redux';
// eslint-disable-next-line
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import './index.css';
import './App.scss';
import './reset.scss';
import {LoginPage} from '@pages/loginPage'


class App extends React.Component {
  render() {
    return (
      <Router>
      <Switch>
        <Route path="/main">
          <LoginPage />
        </Route>
        <Route path="/">
          <LoginPage />
        </Route>
      </Switch>
    </Router>
    );
  }
}

const mapStateToProps = () => ({
});

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
