import { actionCreate } from './actionCreator';

// export const openModal = (stage, data) => {
//     if (stage === 'done') {
//         return actionCreate('OPEN_MODAL', data).done
//     };
//     if (stage === 'start') {
//         return actionCreate('OPEN_MODAL', data).start
//     };
//     if (stage === 'fail') {
//         return actionCreate('OPEN_MODAL', data).fail
//     }
// }


export const OpenModal = {
  start: (data) => actionCreate('OPEN_MODAL', data).start,
  done: (data) => actionCreate('OPEN_MODAL', data).done,
  fail: (data) => actionCreate('OPEN_MODAL', data).fail,

};
