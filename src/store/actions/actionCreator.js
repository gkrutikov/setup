export const actionCreate = (type, data) => ({ start: { type: `${type}_START`, payload: data }, done: { type: `${type}_DONE`, payload: data }, fail: { type: `${type}_FAIL`, payload: data } });
