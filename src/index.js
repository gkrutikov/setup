import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { composeWithDevTools } from "redux-devtools-extension";
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import rootReducer from './store';

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

const NewApp = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

ReactDOM.render(<NewApp />, document.getElementById('root'));

module.hot.accept();