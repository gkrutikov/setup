const {
    override,
    fixBabelImports,
    addBabelPlugins,
    addLessLoader,
    addWebpackPlugin,
    addWebpackAlias,
  } = require('customize-cra');
  const path = require('path');
  const AntdDayjsWebpackPlugin = require('antd-dayjs-webpack-plugin');
  
  const overrideProcessEnv = (value) => (config) => {
    const { plugins } = config;
    const plugin = plugins.find((p) => p.constructor.name === 'DefinePlugin');
    const processEnv = plugin.definitions['process.env'] || {};
  
    plugin.definitions['process.env'] = {
      ...processEnv,
      ...value,
    };
  
    return config;
  };
  
  module.exports = override(
    ...addBabelPlugins(
              ["@babel/plugin-proposal-decorators", { "legacy": true }],
              ["@babel/plugin-proposal-class-properties", { "loose" : true }]
    ),
    fixBabelImports('import', {
      libraryName: 'antd',
      libraryDirectory: 'es',
      style: true,
    }),
    addLessLoader({
      javascriptEnabled: true,
      modifyVars: { '@primary-color': '#1890ff' },
    }),
    addWebpackAlias({
      '@': path.resolve(__dirname, './src'),
      '@components': path.resolve(__dirname, './src/components'),
      '@pages': path.resolve(__dirname, './src/pages'),
      '@utils': path.resolve(__dirname, './src/utils'),
      '@store': path.resolve(__dirname, './src/store'),
    }),
    addWebpackPlugin(new AntdDayjsWebpackPlugin()),
    overrideProcessEnv({
      API_URL: JSON.stringify(process.env.API_URL || '__API_URL'),
    }),
  );